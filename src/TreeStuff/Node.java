package TreeStuff;
import TreeStuff.*;

public class Node {
	private int value, index, leftSum, rightSum, greatestPathSum;
	private Path greatestPath;
	private Node leftNode, rightNode, parentNode;
	
	/**
	 * The constructor creates a new node with the given
	 * value and index, and defaults this new node's greatest
	 * path as just itself.
	 * @param index: (int) The index of this node in the parent array structure
	 * @param value: (int) The value that the node holds
	 */
	public Node(int index, int value) {
		this.value = value;
		this.index = index;
		this.greatestPathSum = value;
		greatestPath = new Path(new Node[] { this } );
	}
	
	/**
	 * This function gets the value of the given node
	 * @return: (int) The integer value of the node
	 */
	public int getValue() { return value; }
	
	/**
	 * Gets the numerical index of this node in its
	 * parent array structure.
	 * @return: (int) The node's index
	 */
	public int getIndex() { return index; }
	
	/**
	 * This function sets the left child node for
	 * this node, and calculates the greatest path
	 * for this node.
	 * @param leftNode: (Node) The given node's new left child node
	 */
	public void setLeftNode(Node leftNode) {
		this.leftNode = leftNode;
		leftSum = value + leftNode.getValue();
		calculateGreatestPath();
	}
	
	/**
	 * This function sets the left child node for
	 * this node, and DOES NOT calculate the greatest path
	 * for this node.
	 * @param leftNode: (Node) The given node's new left child node
	 */
	public void setLeftNodeNoCalculate(Node leftNode) {
		this.leftNode = leftNode;
		leftSum = value + leftNode.getValue();
	}
	
	/**
	 * Gets the left child node of this node
	 * @return: (Node) This node's left child node
	 */
	public Node getLeftNode() { return leftNode; }
	
	/**
	 * This function sets the right child node for
	 * this node, and calculates the greatest path
	 * for this node.
	 * @param rightNode: (Node) The given node's new right child node
	 */
	public void setRightNode(Node rightNode) {
		this.rightNode = rightNode;
		rightSum = value + rightNode.getValue();
		calculateGreatestPath();
	}
	
	/**
	 * This function sets the right child node for
	 * this node, and DOES NOT calculate the greatest path
	 * for this node.
	 * @param rightNode: (Node) The given node's new right child node
	 */
	public void setRightNodeNoCalculate(Node rightNode) {
		this.rightNode = rightNode;
		rightSum = value + rightNode.getValue();
	}
	
	/**
	 * Gets the right child node of this node
	 * @return: (Node) This node's right child node
	 */
	public Node getRightNode() { return rightNode; }
	
	/**
	 * This function sets the parent node for this given node. This
	 * link is not totally necessary for the GreatestPath calculation,
	 * but is a good utility function to have nonetheless.
	 * @param parentNode: (Node) The new parent node of this node
	 */
	public void setParentNode(Node parentNode) { this.parentNode = parentNode; }
	
	/**
	 * Gets the parent node of this given node.
	 * @return: (Node) The parent node of this node
	 */
	public Node getParentNode() { return parentNode; }
	
	/**
	 * Manually sets the greatest path of this node.
	 * @param greatestPath: (Path) The greatest path of this node
	 */
	public void setGreatestPath(Path greatestPath) {
		this.greatestPath = greatestPath;
		greatestPathSum = greatestPath.getPathSum();
	}
	
	/**
	 * This function gets the greatest path of the given node.
	 * @return: (Path) This node's greatest path
	 */
	public Path getGreatestPath() { return greatestPath; }
	
	/**
	 * Manually sets the greatest sum path of this node's
	 * greatest path.
	 * @param greatestPathSum: (int) The sum
	 */
	public void setGreatestPathSum(int greatestPathSum) { this.greatestPathSum = greatestPathSum; }
	
	/**
	 * Gets the sum of this node's greatest path.
	 * @return: (int) The node's greatest path sum
	 */
	public int getGreatestPathSum() { return greatestPathSum; }
	
	/**
	 * Gets the sum of the left child. Not important for
	 * calculating the greatest path.
	 * @return: (int) The sum of the left child.
	 */
	public int getLeftSum() { return leftSum; }
	
	/**
	 * Gets the sum of the right child. Not important for
	 * calculating the greatest path.
	 * @return: (int) The sum of the right child.
	 */
	public int getRightSum() { return rightSum; }
	
	/**
	 * This function calculates the greatest path for this
	 * given node, based on its own value and the greatest
	 * paths of its children nodes.
	 */
	public void calculateGreatestPath() {
		
		// Calculate greatest paths. If the node has no children, the path remains the same
		if (leftNode == null && rightNode == null) {
			greatestPathSum = value;
			return;
		}
		
		// Reset
		Path newGreatestPath = new Path(new Node[] { this } );
		
		/*
		// If all of these conditions are untrue, then the node by itself is still the greatest
		if (value >= value + leftNode.getGreatestPathSum() + leftNode.getGreatestPathSum()) {
			// This node alone is greater than its childrens' paths plus itself
			greatestPath = new Path(new Node[] { this } );
		}
		
		if (value >= value + leftNode.getGreatestPathSum()) {
			// This node alone is greater than its left child's path plus itself
			greatestPath = new Path(new Node[] { this } );
		}
		if (value >= value + rightNode.getGreatestPathSum()) {
			// This node alone is greater than its right child's path plus itself
			greatestPath = new Path(new Node[] { this } );
		}
		*/
		
		// By the time the stack pointer gets here, the greatestPathSum is just "value"
		
		if ((leftNode != null) && (greatestPathSum < value + leftNode.getGreatestPathSum())) {
			// The node plus its left child's path is now this node's greatest path
			// System.out.println("Index: " + getIndex() + " Left... Pathsum " + greatestPathSum + " left " + (value + leftNode.getGreatestPathSum()));
			newGreatestPath = new Path(new Node[] { this,leftNode } );
		}
		if ((rightNode != null) && (greatestPathSum < value + rightNode.getGreatestPathSum())) {
			// The node plus its right child's path is now this node's greatest path
			// System.out.println("Index: " + getIndex() + " Right... Pathsum " + greatestPathSum + " right " + (value + rightNode.getGreatestPathSum()));
			newGreatestPath = new Path(new Node[] { this,rightNode } );
		}
		
		if ((leftNode != null) && (rightNode != null) && (greatestPathSum < value + leftNode.getGreatestPathSum() + rightNode.getGreatestPathSum())) {
			// The node plus the paths of both its children is this node's greatest path
			// System.out.println("Index: " + getIndex() + " Sum... Pathsum " + greatestPathSum + " right " + (value + leftNode.getGreatestPathSum() + rightNode.getGreatestPathSum()));
			newGreatestPath = new Path(new Node[] { this,leftNode,rightNode } );
		}
		
		greatestPath = newGreatestPath;
		greatestPathSum = greatestPath.getPathSum();
	}
}
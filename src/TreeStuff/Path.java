package TreeStuff;
import TreeStuff.*;

public class Path {
	private Node[] path;
	private int[] pathIndices;
	private int pathSum, size;
	
	public Path(Node[] path) {
		this.path = path;
		pathIndices = new int[path.length];
		size = path.length;
		computeSum();
	}
	
	/**
	 * This function computes the sum of all of the values
	 * of all of the nodes in this path.
	 */
	public void computeSum() {
		pathSum = 0;
		for (int i = 0; i < path.length; i++) {
			if (path[i] == null) {
				System.out.println("Index at " + i + " is null.");
			}
			pathSum += path[i].getValue();
			pathIndices[i] = path[i].getIndex();
		}
	}
	
	public int getPathSum() { return pathSum; }
	public Node[] getPathNodes() { return path; }
	public int[] getPathIndices() { return pathIndices; }
	public Node getNodeAtIndex(int index) { return path[index]; }
	public void addNodeToPath(Node node) {
		Node[] newPath = new Node[path.length + 1];
		pathSum = 0;
		pathIndices = new int[newPath.length];
		
		// Compute here instead of externally to avoid two iterations
		for (int i = 0; i < path.length; i++) {
			pathSum += path[i].getValue();
			pathIndices[i] = path[i].getIndex();
			newPath[i] = path[i];
		}
		
		// Append the new node to the end
		pathSum += node.getValue();
		pathIndices[path.length] = node.getIndex();
		newPath[path.length] = node;
		size++;
		path = newPath;
	}
	public int getPathSize() { return size; }
	
	/**
	 * Here we have to assume that no two nodes can have
	 * the same index.
	 */
	public boolean deleteNodeByIndex(int index) {
		Node[] newPath = new Node[path.length - 1];
		int newPathSum = 0;
		int[] newPathIndices = new int[newPath.length];
		boolean found = false;

		// Search for the index
		for (int i = 0; i < path.length; i++) {
			if (path[i].getIndex() == index) {
				found = true;
				// Skip adding
			}
			else {
				// Add to new path
				if (found == true) {
					newPathSum = path[i].getValue();
					newPathIndices[i-1] = path[i].getIndex();
					newPath[i-1] = path[i];
				}
				else {
					newPathSum = path[i].getValue();
					newPathIndices[i] = path[i].getIndex();
					newPath[i] = path[i];
				}
			}
		}
		if (found == true) {
			pathSum = newPathSum;
			pathIndices = newPathIndices;
			path = newPath;
			size--;
			return true;
		}
		else { return false; }
	}
}
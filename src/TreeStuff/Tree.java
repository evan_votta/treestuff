package TreeStuff;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import TreeStuff.*;

/**
 * Implementation of a full and complete binary tree,
 * where nodes are doubly-linked and know their children
 * and parent.
 */
public class Tree {
	Node[] nodes;
	Path greatestPath;
	int size;
	
	/**
	 * This function constructs the tree. We are assuming
	 * that this is a full and complete binary tree.
	 */
	public Tree(int[][] input) {
		// Create the tree, from bottom up, left to right
		size = (int) Math.pow(2,input.length) - 1;
		System.out.println("Rows: " + input.length + ", total size: " + size);
		nodes = new Node[size];
		for (int row = input.length-1; row >= 0; row--) {
			// We are in a row
			for (int col = 0; col < input[row].length; col++) {
				int index = (int) Math.pow(2,row) - 1 + col;
				Node node = new Node(index,input[row][col]);
				// System.out.println("index: " + index + ", row: " + row + ", col: " + col);
				
				if (row < input.length-1) {
					// We are not in a leaf node, set children and parent
					Node leftChild = nodes[index*2+1];
					Node rightChild = nodes[index*2+2];
					
					node.setLeftNode(leftChild);
					node.setRightNode(rightChild);
					
					leftChild.setParentNode(node);
					rightChild.setParentNode(node);
					
					node.setLeftNodeNoCalculate(leftChild);
					node.setRightNodeNoCalculate(rightChild);
				}
				nodes[index] = node;
			}
		}
	}
	
	/**
	 * Polymorphic constructor. Same as the one above it, but
	 * it accepts a File instance, which should be a text file
	 * storing the tree, where each text line in the file is a
	 * comma-delimited list of node values.
	 * @param file: (File) The file that contains the tree
	 * @throws FileNotFoundException: Thrown if we can't find the file.
	 */
	public Tree(File file) throws FileNotFoundException {
		Scanner scan = new Scanner(file);
		
		// First, get the number of rows, and therefore number of nodes
		int numRows = 0;
		while(scan.hasNext()) {
			scan.next();
			numRows++;
		}
		scan.close();
		
		size = (int) Math.pow(2,numRows) - 1;
		nodes = new Node[size];
		
		// Reset scanner
		scan = new Scanner(file);
		StringTokenizer tokenizer;
		
		int index = 0;
		int row = 0;
		while(scan.hasNext()) {
		    // Process the line
			String line = scan.nextLine().trim();
			tokenizer = new StringTokenizer(line,",");

			// Scan through each node
			while(tokenizer.hasMoreTokens()) {
				Node node = new Node(index,Integer.parseInt(tokenizer.nextToken()));
				
				if (row > 0) {
					// We are not in the root node, set children and parent
					Node parentNode;
					if (index % 2 == 1) {
						// Odd number, this node is a left node. Parent index is index-1 / 2
						parentNode = nodes[(index-1) / 2];
						node.setParentNode(parentNode);
						parentNode.setLeftNode(node);
						node.setParentNode(parentNode);
					}
					else {
						// Even number, this node is a right node. Parent index is index-2 / 2
						parentNode = nodes[(index-2) / 2];
						node.setParentNode(parentNode);
						parentNode.setRightNode(node);
						node.setParentNode(parentNode);
					}
				}
				nodes[index++] = node;
				row++;
			}
		}
		scan.close();
	}
	
	/**
	 * By the time we get to this function, the tree is already constructed
	 * and each node already stores its own greatest path, so when we try to find
	 * the tree's overall greatest path, we just have to loop through each node
	 * and compare it to a running highest.
	 */
	public void findGreatestPath() {
		// When starting, set the greatest path as just the bottom right node
		greatestPath = new Path(new Node[] { nodes[size-1] });
		// Iterate from bottom up, left to right
		for (int i = size-1; i >= 0; i--) {
			int greatestPathSum = greatestPath.getPathSum();
			
			if (nodes[i].getGreatestPathSum() >= greatestPathSum) {
				// This node's path is now the greatest
				greatestPath = nodes[i].getGreatestPath();
				greatestPathSum = greatestPath.getPathSum();
			}
		}
	}
	
	/**
	 * This function simply prints out the tree in linear form.
	 * Not very useful.
	 */
	public void printTree() {
		String outputString = "";
		for (int i = 0; i < size; i++) {
			outputString += nodes[i].getValue() + " ";
		}
		System.out.println(outputString);
	}
	
	/**
	 * This function prints information about the greatest contiguous
	 * path, including the path size, and a list of nodes in the path.
	 */
	public void printGreatest() {
		String output = "(Index,Value): {";
		System.out.println("Greatest Path size: " + greatestPath.getPathSize());
		for (int i = 0; i < greatestPath.getPathSize(); i++) {
			output += "(" + greatestPath.getPathNodes()[i].getIndex() + "," + greatestPath.getPathNodes()[i].getValue() + "),";
		}
		output = output.substring(0,output.length() - 1) + "}";
		System.out.println(output);
	}
	
	/**
	 * The main method takes one arg, which is the
	 * path to the file that holds the tree data.
	 * @param args[0]: (String) The path to the data file
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		
		// Check args
		if (args == null || args.length == 0) {
			System.out.println("Need to list the file path!");
			System.exit(1);
		}
		
		// Scan the input file
		for (int i = 0; i < args.length; i++) {
			System.out.println("Beginning Tree # " + (i+1) + "...");
			Tree tree = new Tree(new File(args[i]));
			tree.findGreatestPath();
			tree.printGreatest();
			System.out.println("Done with Tree # " + (i+1) + ".\n");
		}
	}
}